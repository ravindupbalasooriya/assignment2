#include<stdio.h>
int main() {
      double first, second, tempo;
      printf("Enter the 1st number: ");
      scanf("%lf", &first);
      printf("Enter the 2nd number: ");
      scanf("%lf", &second);
      tempo = first;
      first = second;
      second = tempo; 
      printf("\n1st number after the swap = %lf\n", first);
      printf("2nd number after the swap = %lf", second);
      return 0;
}
